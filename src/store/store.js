import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
import plan from './modules/plan'
import checkpoint from './modules/checkpoint'
import place from './modules/place'
import map from './modules/map'
import geolocation from './modules/geolocation'
import window_variables from './modules/window_variables'
import { isMobile } from 'mobile-device-detect';


Vue.use(Vuex)

export default new Vuex.Store({
  // REVIEW: Перенести в отдельный app.store.js
  // REVIEW: Файлы с именем *.store.js
  actions: {

  },
  mutations: {

  },
  state: {  // REVIEW: state вверху
    nested_colors: ['reserv', '#00572b', '#009e4e', '#00d96b', 'red'],
    // isMobile: isMobile,
  },
  getters: {
    nestedColors(state) {
      return state.nested_colors;
    },
    isMobile() {
      return isMobile;
    }
  },
  modules: {
    user, plan, checkpoint, place, map, geolocation, window_variables
  }
})
